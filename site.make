core = 7.x
api = 2

; entityreference_prepopulate
projects[entityreference_prepopulate][type] = "module"
projects[entityreference_prepopulate][download][type] = "git"
projects[entityreference_prepopulate][download][url] = "https://git.uwaterloo.ca/drupal-org/entityreference_prepopulate.git"
projects[entityreference_prepopulate][download][tag] = "7.x-1.1"
projects[entityreference_prepopulate][subdir] = ""

; eva
projects[eva][type] = "module"
projects[eva][download][type] = "git"
projects[eva][download][url] = "https://git.uwaterloo.ca/drupal-org/eva.git"
projects[eva][download][tag] = "7.x-1.2"
projects[eva][subdir] = ""

; og
projects[og][type] = "module"
projects[og][download][type] = "git"
projects[og][download][url] = "https://git.uwaterloo.ca/drupal-org/og.git"
projects[og][download][tag] = "7.x-2.0-beta2"
projects[og][subdir] = ""

; og_extras
projects[og_extras][type] = "module"
projects[og_extras][download][type] = "git"
projects[og_extras][download][url] = "https://git.uwaterloo.ca/drupal-org/og_extras.git"
projects[og_extras][download][tag] = "7.x-1.1"
projects[og_extras][subdir] = ""
